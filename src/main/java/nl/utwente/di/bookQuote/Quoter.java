package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
	static final HashMap<String, Double> priceTable = new HashMap();
	static {
		priceTable.put("1", 10.0);
		priceTable.put("2", 45.0);
		priceTable.put("3", 20.0);
		priceTable.put("4", 35.0);
		priceTable.put("5", 50.0);
	}
	
	double getBookPrice(String isbn) {
		if (priceTable.containsKey(isbn)) {
			return priceTable.get(isbn);
		} else {
			return 0.0;
		}
	}
}
